import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'toMs'
})
export class ToMsPipe implements PipeTransform {

  transform(value: number | undefined, ...args: unknown[]): number {
    if (value == undefined) {
      return 0;
    } else {
      return value * 1000;
    }
  }

}
