import { Component } from '@angular/core';
import {EndpointService, IVisualization} from "./services/endpoint.service";
import {map, mergeMap, Observable, timer} from "rxjs";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'visualization';

  public data$: Observable<IVisualization>;

  public constructor(private endpoint: EndpointService) {
    this.data$ = timer(0, 1000).pipe(
      mergeMap(x => this.endpoint.fetchData()));
  }

}
