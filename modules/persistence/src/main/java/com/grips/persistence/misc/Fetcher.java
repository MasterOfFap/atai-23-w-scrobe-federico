package com.grips.persistence.misc;

import java.util.Optional;

public interface Fetcher<T, ID>  {
    Optional<T> findById(ID id);
}
