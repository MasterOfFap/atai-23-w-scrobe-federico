package com.shared.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Point2dInt {
    int x;
    int y;


    //from java.awt.geom.Point2D
    public static double distance(int var0, int var2, int var4, int var6) {
        var0 -= var4;
        var2 -= var6;
        return Math.sqrt(var0 * var0 + var2 * var2);
    }

    public double distance(Point2dInt var1) {
        int var2 = var1.getX() - this.getX();
        int var4 = var1.getY() - this.getY();
        return Math.sqrt(var2 * var2 + var4 * var4);
    }

    @Override
    public String toString() {
        return "{ x: " + x + ", y: " + y + "}";
    }
}
