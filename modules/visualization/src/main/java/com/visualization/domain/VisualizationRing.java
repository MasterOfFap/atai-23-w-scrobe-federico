package com.visualization.domain;

import com.rcll.domain.RingColor;

public interface VisualizationRing {
    RingColor getRingColor();
    int getRawMaterial();
}
