package com.grips.refbox.msg_handler;

import com.grips.persistence.dao.ZoneDao;
import com.grips.persistence.domain.Zone;
import org.dozer.Mapper;
import org.robocup_logistics.llsf_msgs.ExplorationInfoProtos;
import org.springframework.stereotype.Service;

import java.util.function.Consumer;

@Service
public class ExplorationInfoHandler implements Consumer<ExplorationInfoProtos.ExplorationInfo> {

    private final Mapper mapper;
    private final ZoneDao zoneDao;

    public ExplorationInfoHandler(Mapper mapper, ZoneDao zoneDao) {
        this.mapper = mapper;
        this.zoneDao = zoneDao;
    }

    @Override
    public void accept(ExplorationInfoProtos.ExplorationInfo explorationInfo) {
        for (ExplorationInfoProtos.ExplorationZone curr_exp_zone : explorationInfo.getZonesList()) {
            Zone zone = mapper.map(curr_exp_zone, Zone.class);
            zoneDao.save(zone);
        }
    }
}
