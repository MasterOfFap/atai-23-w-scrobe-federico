package com.grips.monitoring;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.grips.persistence.dao.*;
import com.grips.persistence.domain.*;
import com.grips.scheduler.GameField;
import com.grips.scheduler.api.IScheduler;
import com.grips.scheduler.asp.AspScheduler;
import com.grips.scheduler.exploration.MachineExplorationService;
import com.rcll.domain.MachineName;
import com.rcll.domain.Order;
import com.rcll.refbox.RefboxClient;
import com.visualization.TeamServerVisualization;
import com.visualization.TeamServerVisualizationImpl;
import com.visualization.domain.GamePointsData;
import com.visualization.domain.RobotBeacon;
import com.visualization.domain.VisualizationObservation;
import lombok.SneakyThrows;
import lombok.extern.apachecommons.CommonsLog;
import org.apache.commons.collections.IteratorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.swing.text.html.Option;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@CommonsLog
public class VisualizationController {

    private final String recordingsPath = "/home/peter/team_server/recordings";

    @Autowired
    private BeaconSignalFromRobotDao beaconSignalFromRobotDao;

    @Autowired
    private RefboxClient refboxClient;

    @Autowired
    private LockPartDao lockPartDao;

    @Autowired
    private GameField gameField;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MachineExplorationService explorationService;

    @Autowired
    private SubProductionTaskDao subProductionTaskDao;

    @Autowired
    private RobotObservationDao observationDao;

    @Autowired
    @Qualifier("production-scheduler")
    IScheduler productionScheduler;

    @CrossOrigin
    @RequestMapping("/visualization")
    public TeamServerVisualization getVisualizationData() {
        TeamServerVisualization vis = new TeamServerVisualization();
        List<VisualizationObservation> obs = IteratorUtils.toList(observationDao.findAll().iterator());
        vis.setObservations(obs);
        vis.setExplorationZones(gameField.getAllZones());

        ArrayList<RobotBeacon> beacons = new ArrayList<>();
        vis.setRobot1(createBeacon("1").orElse(null));
        vis.setRobot2(createBeacon("2").orElse(null));
        vis.setRobot3(createBeacon("3").orElse(null));

        if (productionScheduler instanceof AspScheduler) {
            vis.setPlan(((AspScheduler) productionScheduler).getPlan());
            vis.setTimeFromPlan(((AspScheduler)productionScheduler).getPlanTime());
        }
        try {
            vis.setProductOrders(refboxClient.getAllOrders());
        } catch (Exception e) {
            vis.setProductOrders(new ArrayList<>());
        }

        vis.setTimeStamp(refboxClient.getLatestGameTimeInSeconds());

        return vis;
    }

    private Optional<RobotBeacon> createBeacon(String robotId) {
        BeaconSignalFromRobot tmp  = beaconSignalFromRobotDao.findFirstByRobotIdOrderByLocalTimestampDesc(robotId);
        if (tmp == null) {
            return Optional.empty();
        }
        Optional<SubProductionTask> task = subProductionTaskDao.findById((long) tmp.getTaskId());
        String taskName = "";
        if (task.isPresent()) {
            taskName = task.get().getName() + "-" +task.get().getMachine();
            if (task.get().getSide() != null) {
                taskName += "-" + task.get().getSide().name();
            }
        }
        return Optional.of(new RobotBeacon(tmp.getLocalTimestamp(), tmp.getRobotName(), tmp.getTeamName(), taskName, Integer.parseInt(tmp.getRobotId())));
    }


    @CrossOrigin
    @RequestMapping("/recordings_list")
    public List<String> recordingsList() throws IOException {
        List<String> returner = new ArrayList<>();
        try (Stream<Path> paths = Files.walk(Paths.get(recordingsPath))) {
            paths
                    .filter(Files::isRegularFile)
                    .filter(x -> x.getFileName().toString().endsWith(".json"))
                    .map(x -> x.getFileName().toString())
                    .forEach(returner::add);
        }
        return returner;
    }

    @CrossOrigin
    @RequestMapping("/recording/{name}")
    public String recordingsList(@PathVariable String name) throws IOException {
        return new String(Files.readAllBytes(Paths.get(recordingsPath + "/" + name)));
    }

    @SneakyThrows
    @CrossOrigin
    @RequestMapping("/gamepoints")
    public List<GamePointsData> gamePoints() {
        return recordingsList().stream()
                .filter(name -> name.startsWith("pro"))
                .map(name -> {
                    try {
                        TeamServerVisualizationImpl[] viz;
                        viz = objectMapper.readValue(new File(recordingsPath + "/" + name), TeamServerVisualizationImpl[].class);
                        GamePointsData data = new GamePointsData();
                        data.setGameName(name);
                        TeamServerVisualizationImpl last = viz[viz.length - 1];
                        data.setPointMagenta(last.getGameState().getPointsMagenta());
                        data.setPointsCyan(last.getGameState().getPointsCyan());
                        return data;
                    } catch (IOException e) {
                        log.error("Error reading: ", e);
                    }
                    //get points from last entry only!
                    return null;
                })
                .collect(Collectors.toList());
    }
}
