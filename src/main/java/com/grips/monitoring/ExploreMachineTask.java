package com.grips.monitoring;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExploreMachineTask {
    private Long robotId;
    private String machine;
    private String waypoint;
    private boolean rotate;
}
