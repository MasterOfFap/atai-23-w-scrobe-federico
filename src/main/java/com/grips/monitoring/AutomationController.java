package com.grips.monitoring;

import com.rcll.refbox.RefboxClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigInteger;

@RestController
public class AutomationController {

    @Autowired
    private RefboxClient refboxClient;

    @RequestMapping("/game/phase")
    public String getGamePhase() {
        return refboxClient.getGamePhase().name();
    }

    @RequestMapping("/game/time")
    public BigInteger gameTime() {
        return new BigInteger(refboxClient.getLatestGameTimeInSeconds().toString());
    }

    @RequestMapping("/game/state")
    public String getGameState() {

        return refboxClient.getGameState().name();
    }
}
