package com.grips.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "gameconfig.productiontimes")
public class ProductionTimesConfig {
    private Integer bufferTask;
    private Integer moveTask;
    private Integer normalTask;
}
