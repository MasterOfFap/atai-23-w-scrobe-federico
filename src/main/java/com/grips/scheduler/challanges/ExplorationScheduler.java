package com.grips.scheduler.challanges;

import com.grips.scheduler.api.IScheduler;
import com.rcll.domain.Peer;
import lombok.extern.apachecommons.CommonsLog;
import org.robocup_logistics.llsf_msgs.AgentTasksProtos;
import org.robocup_logistics.llsf_msgs.BeaconSignalProtos;
import org.robocup_logistics.llsf_msgs.MachineInfoProtos;

@CommonsLog
public class ExplorationScheduler implements IScheduler {

    @Override
    public void handleBeaconSignal(BeaconSignalProtos.BeaconSignal beacon_signal, int robotId, Peer robot) {
        log.error("NavigationScheduler::handleBeaconSignal not implemented!");
    }

    @Override
    public boolean handleRobotTaskResult(AgentTasksProtos.AgentTask prsTask) {
        log.error("NavigationScheduler::handleBeaconSignal not implemented!");
        return false;
    }

    @Override
    public void handleMachineInfo(MachineInfoProtos.Machine machine) {
        log.error("NavigationScheduler::handleBeaconSignal not implemented!");
    }
}
