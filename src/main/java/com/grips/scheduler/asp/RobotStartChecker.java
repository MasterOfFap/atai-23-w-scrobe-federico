package com.grips.scheduler.asp;

import com.grips.persistence.dao.BeaconSignalFromRobotDao;
import com.grips.persistence.dao.SubProductionTaskDao;
import com.grips.persistence.domain.BeaconSignalFromRobot;
import com.grips.persistence.domain.SubProductionTask;
import com.grips.persistence.domain.SubProductionTaskBuilder;
import com.grips.robot.RobotClientWrapper;
import com.rcll.refbox.RefboxClient;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.stereotype.Service;

@Service
@CommonsLog
public class RobotStartChecker {
    private final BeaconSignalFromRobotDao beaconSignalFromRobotDao;
    private final RefboxClient refboxClient;
    private final SubProductionTaskDao subProductionTaskDao;
    private final RobotClientWrapper robotClientWrapper;
    private boolean robot1Started;
    private boolean robot2Started;
    private boolean robot3Started;
    public RobotStartChecker(BeaconSignalFromRobotDao beaconSignalFromRobotDao,
                             RefboxClient refboxClient,
                             SubProductionTaskDao subProductionTaskDao,
                             RobotClientWrapper robotClientWrapper) {
        this.beaconSignalFromRobotDao = beaconSignalFromRobotDao;
        this.refboxClient = refboxClient;
        this.subProductionTaskDao = subProductionTaskDao;
        this.robotClientWrapper = robotClientWrapper;
        robot1Started = false;
        robot2Started = false;
        robot3Started = false;
    }

    public void leaveStartingArea(int robotId) {
        SubProductionTask moveTask = SubProductionTaskBuilder.newBuilder()
                .setName("MoveToZone")
                .setState(SubProductionTask.TaskState.INWORK)
                .setType(SubProductionTask.TaskType.MOVE)
                .setOrderInfoId(null)
                .build();
        moveTask.setRobotId(robotId);
        subProductionTaskDao.save(moveTask);

        String colorprefix = refboxClient.isCyan() ? "C_" : "M_";

        if (robotId == 2)
            robotClientWrapper.sendMoveToZoneTask((long) robotId, moveTask.getId(), colorprefix + "Z52");
        else
            //robotClientWrapper.sendMoveToZoneTask(Long.valueOf(robotId), moveTask.getId(), colorprefix + "Z51");
            robotClientWrapper.sendMoveToZoneTask((long) robotId, moveTask.getId(), colorprefix + "Z52");
    }


    public boolean checkIfShouldStart(int robotId) {
        BeaconSignalFromRobot from1 = beaconSignalFromRobotDao.findFirstByRobotIdOrderByLocalTimestampDesc("1");
        BeaconSignalFromRobot from2 = beaconSignalFromRobotDao.findFirstByRobotIdOrderByLocalTimestampDesc("2");
        BeaconSignalFromRobot from3 = beaconSignalFromRobotDao.findFirstByRobotIdOrderByLocalTimestampDesc("3");

        if (robotId == 1) {
            robot1Started = true;
            return true;
        } else {
            if (robotId == 2) {
                if (from1 == null)
                    return false;
                if (robot2Started) {
                    return true;
                }
                boolean checkPose = checkPose(from1.getPoseX(), from1.getPoseY(), from2.getPoseX(), from2.getPoseY());
                if (checkPose && !robot2Started) {
                    robot2Started = true;
                }
                return checkPose;
            } else { //robotId = 3
                if (from2 == null)
                    return false;
                if (robot3Started) {
                    return true;
                }
                boolean checkPose = checkPose(from2.getPoseX(), from2.getPoseY(), from3.getPoseX(), from3.getPoseY());
                if (checkPose) {
                    robot3Started = true;
                }
                return checkPose;
            }
        }
    }

    private boolean checkPose(double poseX1, double poseY1, double poseX2, double poseY2) {
        double distance = calculateDistance(poseX1, poseY1, poseX2, poseY2);
            if (distance < 1.2) {
                //log.info("NOT STARTING distance between the two robots is " + distance);
                return false;
            } else {
                //log.info("distance between the two robots is " + distance);
                return true;
            }
    }

    private double calculateDistance(double poseX1, double poseY1, double poseX2, double poseY2) {
        return Math.sqrt(((poseX1 - poseX2) * (poseX1 - poseX2)) + ((poseY1 - poseY2) * (poseY1 - poseY2)));
    }
}
